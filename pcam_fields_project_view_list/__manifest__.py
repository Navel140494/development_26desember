# -*- coding: utf-8 -*-
{
    'name': "Add fields in list view Project",

    'summary': """
        Add fields on List View in Project App""",

    'description': """
        Add Progress, Customer and Vendor fields in the List View (View Tree). 
    """,

    'author': "Port Cities Americas",
    'website': "ttps://www.portcities.net/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Project',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','project','pcam_project_manager'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}