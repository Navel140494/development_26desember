# -*- coding: utf-8 -*-
{
    'name': "Customatization Task",

    'summary': """
        This module realize the customatization on the Project and Task modules""",

    'description': """
        
    """,

    'author': "Port Cities Americas",
    'website': "https://www.portcities.net/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Project',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'project', 'project_breakdown', 'hr_timesheet', 'pci_kpi_okr', 'web'],

    # always loaded
    'data': [
        #'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        #'views/subtask.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}