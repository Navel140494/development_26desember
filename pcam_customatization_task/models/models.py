# -*- coding: utf-8 -*-

from odoo import models, fields, api,_

class customatizationTask(models.Model):
	_inherit = 'project.task'
	_description = 'Module for multi task'
	
	task_type = fields.Selection([('key_result', 'Milestone'),('product', 'Deliverable'),('child', 'Subtask')], string='Task Type', default='key_result')
	subtype_subtask = fields.Selection(selection=[('development', 'Development'),('document', 'Document'),('other', 'Other')], string='Sub-Type', copy=False)
	date_start = fields.Date(string='Start Date')
	multi_subtasks_ids = fields.One2many('project.task', 'id', "Children")