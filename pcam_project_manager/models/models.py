# -*- coding: utf-8 -*-

from odoo import models, fields, api

class inherit_progress_project(models.Model):

	_inherit = 'project.project'

	pcam_project_progress = fields.Float(string = 'Progress', compute="inherit_task_project_progress")
	max_rate = fields.Integer(string='Maximum rate', default=100)
	task_checklist = fields.Many2many('project.task', string='Check List')

	@api.one
	@api.depends('pcam_project_progress', 'task_checklist')
	def inherit_task_project_progress(self):
		record_average_task = self.env['project.task'].search_count([('project_id', '=', self.id), ('task_type', '!=', 'key_result')])
		records_task = self.env['project.task'].search([('project_id', '=', self.id), ('task_type', '!=', 'key_result')])
		lines = self.env['project.task'].search([])
		suma = 0 
		
		for line in self:
			total_amount = 0.0
			div = 0.0
			for item in records_task:
				if record_average_task != 0:
					total_amount += item.pcam_progress_task
					div = (total_amount)/record_average_task
					line.pcam_project_progress = div
				else: 
					line.pcam_project_progress = 0

class inherit_progress_task(models.Model):

	_inherit = 'project.task'

	pcam_progress_task = fields.Float(string = 'Progress(%): ', default='0.0')
	progres_task_milstone = fields.Float(string = 'Progress: ', default='0.0', compute="compute_milestone")

	@api.multi
	@api.depends('progres_task_milstone')
	def compute_milestone(self):
		record_project = self.env['project.project'].search([('id', '=', self.project_id.id)])
		pass_val = 0
		for rec in self:
			for res in record_project:
				if rec.task_type == 'key_result':
					pass_val = res.pcam_project_progress
					rec.progres_task_milstone = pass_val
				else:
					rec.progres_task_milstone = 0.0