# -*- coding: utf-8 -*-
{
    'name': "Project Manager in View Kanban",

    'summary': """Add a Project Manager in view kanban in the Project App""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Port Cities Americas",
    'website': "https://www.portcities.net/",

    'category': 'Project',
    'version': '0.1',

    'depends': ['base','project', 'pci_kpi_okr', 'pcam_customatization_task','analytic'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],

    'demo': [
        'demo/demo.xml',
    ],
}