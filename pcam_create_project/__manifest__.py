# -*- coding: utf-8 -*-
{
    'name': "Customatization create project",

    'summary': """Add fields in view kanban at create project""",

    'description': """
        Customatization for view kanban project at create an new project
    """,

    'author': "Port Cities Americas",
    'website': "https://erp.portcities.net/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Project',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','project','project_reviewer'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}