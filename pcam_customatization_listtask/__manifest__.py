# -*- coding: utf-8 -*-
{
    'name': "Customatization for task list",

    'summary': """Customatization for task list view""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Port Cities",
    'website': "https://erp.portcities.net/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Task',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'project', 'pcam_customatization_task', 'project_mytask'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/view_search.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}