# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, timedelta

class customatizationTimesheet(models.Model):
	_inherit='project.task'

	childs_task_ids = fields.One2many('project.task', 'parent_task_id', "Children")
	calculate_remaining_hours = fields.Float(string = 'Remaining hours', compute='calculate_reaining') 

	@api.depends('calculate_remaining_hours')
	def calculate_reaining(self):
		calculate_time = 0
		if self:
			calculate_time = (self.planned_hours - self.time_spent)
			self.calculate_remaining_hours = calculate_time
		else:
			self.calculate_remaining_hours = 0

	