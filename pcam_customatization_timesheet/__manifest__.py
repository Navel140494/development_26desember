# -*- coding: utf-8 -*-
{
    'name': "Customatization Timesheet",

    'summary': """Move the Timesheet table from """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Port Cities Americas",
    'website': "https://www.portcities.net/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Project',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
        'project',
        'hr_timesheet',
        'project_myproject_menu',
        'project_breakdown',
        'pci_kpi_okr',
        ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}